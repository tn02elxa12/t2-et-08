/*double FV(double rate, unsigned int nperiods, double PV) - Calculates and returns the Future Value of an investment based on the compound interest formula FV = PV * (1+rate)nperiods*/
#include <stdio.h>
double power(int x,int n){
    double p=1;
    while(n>0){
        p = p*x;
        n--;
    }
    return p;
}
int main()
{
    int n;
    float PV,rate,fv;
    printf("Enter the future value FV: ");
    scanf("%f",&fv);
    printf("\n Enter the rate: ");
    scanf("%f",&rate);
    printf("\n Enter the period of investment:  ");
    scanf("%d",&n);
    rate = power(1+rate,n);
    PV = fv/rate;
    printf("\n present value of investment is %f",PV);
    return 0;
}