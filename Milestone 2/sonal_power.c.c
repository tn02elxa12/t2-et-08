#include <stdio.h>
double power(int x,int n){
    double p=1;
    while(n>0){
        p = p*x;
        n--;
    }
    return p;
}
int main()
{
    int x,n;
    printf("ENTER A NUMBER TO CALCULATE POWER:");
    scanf("%d",&x);
    printf("ENTER POWER: ");
    scanf("%d",&n);
    printf("pow(x,n) = %.2f",power(x,n));
    return 0;
}