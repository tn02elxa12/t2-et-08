/*double FV(double rate, unsigned int nperiods, double PV) - 
Calculates and returns the Future Value of an investment based on the 
compound interest formula FV = PV * (1+rate)nperiods*/
#include <stdio.h>
double power(int x,int n){
    double p=1;
    while(n>0){
        p = p*x;
        n--;
    }
    return p;
}
int main()
{
    int n;
    float PV,rate,fv;
    printf("Enter the Present value PV: ");
    scanf("%f",&PV);
    printf("\n Enter the rate: ");
    scanf("%f",&rate);
    printf("\n Enter the period of investment:  ");
    scanf("%d",&n);
    rate = power(1+rate,n);
    fv = PV*rate;
    printf("\n future value of investment is %f",fv);
    return 0;
}