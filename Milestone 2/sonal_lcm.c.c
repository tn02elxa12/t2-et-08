//lcm of given 3 numbers
#include <stdio.h>
int main()
{
    int a,b,c,largest,i=2;
    printf("ENTER 3 NUMBERS:");
    scanf("%d %d %d",&a,&b,&c);
    //GET LARGEST NUMBER
    largest = (a>b)?((a>c)?a:c):((b>c)?b:c);
    while(1){
        if(largest%a==0 && largest%b==0 && largest%c==0){
            printf("\n LCM of given numbers is %d",largest);
            break;
        }
        largest = largest*i;
        i++;
    }

    return 0;      
}