#include <stdio.h>
int is_prime(unsigned int n){
    int i;
    for(i=2;i<n/2;i++){
        if(n%i==0)
            return 0;
    }
    return 1;
}
int main()
{
    int x,n;
    printf("Enter a number:");
    scanf("%d",&n);
    x = is_prime(n);
    if(x==1)
        printf("%d is a prime number ",n);
    else
        printf("%d is a non-prime number ",n);
    return 0;
}