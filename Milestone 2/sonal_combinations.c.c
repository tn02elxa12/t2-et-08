//nCr - Combinations
#include <stdio.h>
int fact(int n){
    int p=1;
    while(n>0){
        p = p*n;
        n--;
    }
    return p;
}
int main()
{
    int n,r;
    float C;

    printf("\n ENTER VALUE OF n:  ");
    scanf("%d",&n);
    
    printf("\n ENTER VALUE OF r:  ");
    scanf("%d",&r);
    
    C = fact(n)/(fact(r)*fact(n-r));
    
    printf("\n %dC%d = %.2f",n,r,C);
    
    return 0;
}