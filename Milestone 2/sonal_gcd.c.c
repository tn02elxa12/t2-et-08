//int gcd(int a, int b) - Calculates and returns the Greatest Common divisor of a and b
#include <stdio.h>
int gcd(int a,int b){
    
    if(a>b){
        if(a%b==0)
            return b;
        return(gcd(a-b,b));    
        }
    else{
        if(b%a==0)
        return a;
        return(gcd(b-a,a));    
    }    
    return 1;   
}
int main()
{
    int a,b,g;
    printf("\n ENTER NUMBERS:");
    scanf("%d %d",&a,&b);
    g = gcd(a,b);
    printf("\n gcd is %d",g);
    return 0;   
}