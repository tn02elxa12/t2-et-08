//nPr - Permutations
#include <stdio.h>
int fact(int n){
    int p=1;
    while(n>0){
        p = p*n;
        n--;
    }
    return p;
}
int main()
{
    int n,r;
    float C;

    printf("\n Enter n:  ");
    scanf("%d",&n);
    
    printf("\n Enter r:  ");
    scanf("%d",&r);
    
    C = fact(n)/fact(n-r);
    
    printf("\n %dP%d = %.2f",n,r,C);
    
    return 0;
}