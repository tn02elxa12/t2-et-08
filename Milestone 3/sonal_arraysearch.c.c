//Create a program to input a set of integers and then search the array to see if a particular integer exists in the array
#include <stdio.h>
int main()
{
    int a[30],i,n,c,j,temp;
    printf("enter number of elements in array : ");
    scanf("%d",&n);
    printf("\n enter elements of array: ");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    printf("\n Given array: ");
    for(i=0;i<n;i++)
    printf("%d ",a[i]);
    printf("\n Enter number you want to find ");
    scanf("%d",&c);
    for(i=0;i<n;i++)
{
        if(a[i]==c){
            printf("\n value %d is found at position %d ",c,(i+1));
            break;
        }
    }
    if(i>=n)
        printf("\n %d not found",c);
        
    return 0;      
}