//Create a program to input a set of integers and sort them in ascending or descending order depending on users choice
#include <stdio.h>
int main()
{
    int a[30],i,n,c,j,temp;
    printf("ENTER NUMBER OF ELEMENTS IN ARRAY:");
    scanf("%d",&n);
    printf("\nENTER ELEMENTS OF ARRAY:");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    printf("\n Given array: ");
    for(i=0;i<n;i++)
        printf("%d ",a[i]);
        printf("\n Enter 1 to sort ascending or enter 2 to sort descending ");
    scanf("%d",&c);
    switch(c){
        case 1: for(i=0;i<n-1;i++){
            for(j=i+1;j<n;j++){
                if(a[j]<a[i]){
                    temp = a[i];
                    a[i]=a[j];
                    a[j] = temp;
                }
            }
        }
        break;
        
        case 2: for(i=0;i<n-1;i++){
            for(j=i+1;j<n;j++){
                if(a[j]>a[i]){
                    temp = a[i];
                    a[i]=a[j];
                    a[j] = temp;
                }
            }
        }
        break;
        
        default: printf("\n invalid choice");
            break;
    }
    
    printf("\n new array: ");
    for(i=0;i<n;i++)
        printf("%d ",a[i]);
    

    return 0;    
    
}